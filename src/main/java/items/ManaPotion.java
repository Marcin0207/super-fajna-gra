package items;

import character.Player;

public class ManaPotion extends Item implements Usable{

    private int restoredHpAmount;

    public ManaPotion(String name, double price) {
        super(name, price);
    }
    @Override
    public void use(Player player,int restoredMpAmount){

        int maxMp = player.getDerivativeStats().getMaxMp();
        int currentMp = player.getDerivativeStats().getCurrentMp();

        if(currentMp + restoredHpAmount >= maxMp){
            player.getDerivativeStats().setCurrentHp(maxMp);
        }else {
            player.getDerivativeStats().setCurrentHp(currentMp+restoredHpAmount);
        }
    }

}