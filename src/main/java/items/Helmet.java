package items;

import character.Player;

public class Helmet extends Item implements Equipable {

    int defence;

    public Helmet(String name, double price, int defence) {
        super(name, price);
        this.defence = defence;
    }
    @Override
    public void equip(Player player){
        player.getEq().setHelmet(this);
    }

}
