package items;

import character.Player;

public class BodyArmor extends Item implements Equipable {

    private int defence;

    public BodyArmor (String name, double price, int defence){
        super(name, price);
        this.defence = defence;
    }

    @Override
    public void equip(Player player){
        player.getEq().setArmor(this);
    }

}
