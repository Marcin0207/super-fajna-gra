package items;

import character.Player;

public class HealthPotion extends Item implements Usable{

    private int restoredHpAmount;

    public HealthPotion(String name, double price) {
        super(name, price);
    }
    @Override
    public void use(Player player,int restoredHpAmount){
       int maxHp = player.getDerivativeStats().getMaxHp();
       int currentHp = player.getDerivativeStats().getCurrentHp();

       if(currentHp + restoredHpAmount >= maxHp){
           player.getDerivativeStats().setCurrentHp(maxHp);
       }else {
           player.getDerivativeStats().setCurrentHp(currentHp+restoredHpAmount);
       }

    }

}
