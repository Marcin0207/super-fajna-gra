package items;

import character.Player;

public class Bracelet extends Item implements Equipable {

    int defence;

    public Bracelet(String name, double price, int defence) {
        super(name, price);
        this.defence = defence;
    }

    @Override
    public void equip(Player player){
        player.getEq().setBracelet(this);
    }


}

