package items;

import character.Player;

public interface Usable<T> {

    void use(Player player, int value);

}
