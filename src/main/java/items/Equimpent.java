package items;

public class Equimpent {

    private Weapon weapon;
    private Helmet helmet;
    private BodyArmor armor;
    private Bracelet bracelet;
    private Trinket trinket;

    public Weapon getWeapon() {
        return weapon;
    }

    public void setWeapon(Weapon weapon) {
        this.weapon = weapon;
    }

    public Helmet getHelmet() {
        return helmet;
    }

    public void setHelmet(Helmet helmet) {
        this.helmet = helmet;
    }

    public BodyArmor getArmor() {
        return armor;
    }

    public void setArmor(BodyArmor armor) {
        this.armor = armor;
    }

    public Bracelet getBracelet() {
        return bracelet;
    }

    public void setBracelet(Bracelet bracelet) {
        this.bracelet = bracelet;
    }

    public Trinket getTrinket() {
        return trinket;
    }

    public void setTrinket(Trinket trinket) {
        this.trinket = trinket;
    }

}
