package items;

import character.Player;

public class Trinket extends Item implements Equipable{

    public Trinket(String name, double price) {
        super(name, price);
    }


    @Override
    public void equip(Player player){
        player.getEq().setTrinket(this);
    }

}
