package items;

import character.Player;

public interface Equipable {

   void equip(Player player);

}
