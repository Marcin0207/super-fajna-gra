package items;

import character.Player;

public class Weapon extends Item implements Equipable {

    private int attackPower;

    public Weapon(String name, double price, int attackPower) {
        super(name, price);
        this.attackPower = attackPower;
    }
    @Override
    public void equip(Player player){
        player.getEq().setWeapon(this);
    }

}
