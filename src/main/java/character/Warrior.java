package character;
import items.HealthPotion;
import items.Item;
import items.Weapon;

import java.util.ArrayList;
import java.util.Iterator;

public class Warrior extends Player {

    public Warrior (String name, String race, char gender, CharacterClass characterClass) {
        super(name, race, gender);
        this.characterClass = CharacterClass.WARRIOR;

        MainStats mainStats = new MainStats(this.characterClass);
        DerivativeStats derivativeStats = new DerivativeStats(mainStats);

        bagpack = new ArrayList<>();
        initializeBagpack();
        initializeEquipment();
    }


    @Override
    protected void initializeBagpack() {

        for (int i = 0; i < 5; i++) {
            HealthPotion potion = new HealthPotion("Potion",50);
            bagpack.add(potion);
        }
        Weapon broadSword = new Weapon ("Broad Sword", 330, 12);
        bagpack.add(broadSword);
    }

    @Override
    protected void initializeEquipment() {
        Iterator<Item> iter = bagpack.iterator();

        while(iter.hasNext()) {
            System.out.println(iter);
        }

    }
}
