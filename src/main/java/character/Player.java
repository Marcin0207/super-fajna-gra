package character;

import items.Equimpent;
import items.Item;

import java.util.ArrayList;

public abstract class Player {

    protected CharacterClass characterClass;
    private String name, race;
    private char gender;

    private MainStats mainStats;
    private DerivativeStats derivativeStats;
    private Equimpent eq;
    ArrayList<Item> bagpack;

    public Player(String name, String race, char gender) {
        if (gender == 'M' || gender == 'K') {
            this.gender = gender;
        } else {
            System.out.println("Są tylko dwie płcie");
            throw new IllegalArgumentException("Błędne dane");
        }
        this.name = name;
        this.race = race;

    }

    protected abstract void initializeBagpack();

    protected abstract void initializeEquipment();


    public CharacterClass getCharacterClass() {
        return characterClass;
    }

    public void setCharacterClass(CharacterClass characterClass) {
        this.characterClass = characterClass;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRace() {
        return race;
    }

    public void setRace(String race) {
        this.race = race;
    }

    public char getGender() {
        return gender;
    }

    public void setGender(char gender) {
        this.gender = gender;
    }

    public MainStats getMainStats() {
        return mainStats;
    }

    public void setMainStats(MainStats mainStats) {
        this.mainStats = mainStats;
    }

    public DerivativeStats getDerivativeStats() {
        return derivativeStats;
    }

    public void setDerivativeStats(DerivativeStats derivativeStats) {
        this.derivativeStats = derivativeStats;
    }

    public Equimpent getEq() {
        return eq;
    }

    public void setEq(Equimpent eq) {
        this.eq = eq;
    }

    public ArrayList<Item> getBagpack() {
        return bagpack;
    }

    public void setBagpack(ArrayList<Item> bagpack) {
        this.bagpack = bagpack;
    }
}
