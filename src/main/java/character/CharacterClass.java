package character;

public enum CharacterClass {

    WARRIOR(24,21,12,18,20),
    THIEF(20,23,15,20,18), //21,23,18,23,18
    MAGE(16,19,25,16,18); //12,19,24,16,15

        private final int startStr;
        private final int startDex;
        private final int startInt;
        private final int startSpee;
        private final int startStam;

            CharacterClass(int strength, int dexterity, int inteligence, int speed, int stamina){
                this.startStr = strength;
                this.startDex = dexterity;
                this.startInt = inteligence;
                this.startSpee = speed;
                this.startStam = stamina;
            }

    public int getStartStr() {
        return startStr;
    }

    public int getStartDex() {
        return startDex;
    }

    public int getStartInt() {
        return startInt;
    }

    public int getStartSpee() {
        return startSpee;
    }

    public int getStartStam() {
        return startStam;
    }
}
