package character;

public class DerivativeStats {

    int[] hpModTab = {250, 314, 382, 454, 530, 610, 694, 782, 874, 970};
    int[] mpModTab = {200, 206, 212, 219, 226, 234, 242, 250, 259, 268};

    private int maxHp;
    private int maxMp;
    private int currentHp;
    private int currentMp;
    private int gaugelength;


    public DerivativeStats(MainStats mainStats) {

        this.maxHp = mainStats.getStrength() * hpModTab[mainStats.getLevel()] / 50;
        this.maxMp = mainStats.getIntelligence() + mpModTab[mainStats.getLevel()] / 100;
        this.currentHp = maxHp;
        this.currentMp = maxMp;
        this.gaugelength = (101 - mainStats.getSpeed());


    }

    public int getMaxHp() {
        return maxHp;
    }

    public void setMaxHp(int maxHp) {
        this.maxHp = maxHp;
    }

    public int getMaxMp() {
        return maxMp;
    }

    public void setMaxMp(int maxMp) {
        this.maxMp = maxMp;
    }

    public int getCurrentHp() {
        return currentHp;
    }

    public void setCurrentHp(int currentHp) {
        this.currentHp = currentHp;
    }

    public int getCurrentMp() {
        return currentMp;
    }

    public void setCurrentMp(int currentMp) {
        this.currentMp = currentMp;
    }

    public int getGaugelength() {
        return gaugelength;
    }

    public void setGaugelength(int gaugelength) {
        this.gaugelength = gaugelength;
    }
}
