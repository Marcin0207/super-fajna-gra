package character;

public class MainStats {

    int maxStat = 100;

    private short level;
    private int strength;
    private int dexterity;
    private int intelligence;
    private int speed;
    private int stamina;


    public MainStats(CharacterClass characterClass) {
        this.strength = characterClass.getStartStr();
        this.dexterity = characterClass.getStartDex();
        this.intelligence = characterClass.getStartInt();
        this.speed = characterClass.getStartSpee();
        this.stamina = characterClass.getStartStam();
        this.level = 1;

    }

    public short getLevel() {
        return level;
    }

    public int getStrength() {
        return strength;
    }

    public void setStrength(int strength) {
        this.strength = strength;
    }

    public int getDexterity() {
        return dexterity;
    }

    public void setDexterity(int dexterity) {
        this.dexterity = dexterity;
    }

    public int getIntelligence() {
        return intelligence;
    }

    public void setIntelligence(int intelligence) {
        this.intelligence = intelligence;
    }

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public int getStamina() {
        return stamina;
    }

    public void setStamina(int stamina) {
        this.stamina = stamina;
    }


}
